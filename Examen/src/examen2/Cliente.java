package examen2;

import java.util.ArrayList;

public class Cliente extends Thread {
	private Recursocompartido Caja;
	private ArrayList<Prenda> prendas;

	public Cliente(Recursocompartido caja) {
		super();
		this.Caja = caja;
		this.prendas = new ArrayList<Prenda>();
	}

	public void run() {

		Integer contador = 0;
		Prenda prenda11;

		while (contador < 2) {
			prenda11 = this.getCaja().retiro();
			if (prenda11 != null) {

				getPrendas().add(prenda11);

			} else {
				contador++;

			}

		}
		generoInforme();

	}

	public void generoInforme() {

		Double preciototal = 0.00;
		for (Prenda prenda : prendas) {

			System.out.println("Ticket Prendas vendidas  ");

			System.out.println("Prendas ");
			System.out.println("Prenda 1 : " + "Marca " + prenda.getMarca() + "Tipo: " + prenda.getTipo() + "  Talle: "
					+ prenda.getTalle() + " Prencio : $ " + prenda.getPrecio());

			preciototal = preciototal + prenda.getPrecio();
			System.out.println("Monto Total : $" + preciototal);
			Integer cant = 0;
			Integer cat = 0;
			Integer cants = 0;

			if (prenda.getCodigo() == 1111) {
				cant = cant + 1;
				System.out.println(" se vendieron " + cant + " Pares de Calzado");
			} else if (prenda.getCodigo() == 1110) {
				cat = cat + 1;
				System.out.println("se vendieron " + cat + " Remeras ");
			} else
				cants = cants + 1;
			System.out.println("se vendieron " + cants + " Pantalones");

		}

	}

	public Recursocompartido getCaja() {
		return Caja;
	}

	public void setCaja(Recursocompartido caja) {
		Caja = caja;
	}

	public ArrayList<Prenda> getPrendas() {
		return prendas;
	}

	public void setPrendas1(ArrayList<Prenda> prendas) {
		this.prendas = prendas;
	}
}
