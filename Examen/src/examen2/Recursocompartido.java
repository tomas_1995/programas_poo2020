package examen2;

import java.util.ArrayList;

public class Recursocompartido {
	private ArrayList<Prenda> lista;

	
	public Recursocompartido() {
		super();
		this.lista = new ArrayList<Prenda>();
	}



	public synchronized void almaceno(Prenda prenda) {

		while (this.getLista().size() >= 20) {
			try {

				System.out.println("Almacen lleno");
				System.out.println(
						"El Empleado" + Thread.currentThread().getName() + " esta esperando espacio de almacenamiento");
				wait();

			} catch (InterruptedException e) {
				e.printStackTrace();
				// TODO: handle exception

			}

		}

		this.getLista().add(prenda);
		notifyAll();
		if (prenda != null) {
			System.out.println("El Empleado" + Thread.currentThread().getName() + " almaceno la prenda"+prenda.getMarca());

		}
	}

	public synchronized Prenda retiro() {
		while (this.getLista().size() == 0) {
			try {

				System.out.println("Almacen vacio, Esperando...");
				System.out.println("Cliente" + Thread.currentThread().getName() + "esta esperando prenda en almacen ");
				wait();

			} catch (InterruptedException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		Prenda compro = this.getLista().remove(0);
		

		if (compro != null) {
			System.out.println("El Cliente " + Thread.currentThread().getName() + "  compro la prenda");

		}

		notifyAll();
		return compro;

	}
	public ArrayList<Prenda> getLista() {
		return lista;
	}

	public void setLista(ArrayList<Prenda> lista) {
		this.lista = lista;
	}
}
