package examen2;

public class Prenda {
	private String marca;
	private Integer talle;
	private String tipo;
	private Double precio;
	private Integer codigo;
	
	
	public Prenda() {
		super();
		this.setMarca(marca);
		this.setTalle(talle);
		this.setTipo(tipo);
		this.setPrecio(precio);
		this.setCodigo(codigo);
		
		
	}
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Integer getTalle() {
		return talle;
	}
	public void setTalle(Integer talle) {
		this.talle = talle;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	
	

}
