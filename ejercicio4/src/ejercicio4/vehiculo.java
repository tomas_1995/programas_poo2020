package ejercicio4;

public abstract class vehiculo {
	 private String patente;
	 private String modelo;
	 private String averia;
	 private String tipo;
	 private Integer cant_hs;
	 
	public String getPatente() {
		return patente;
	}
	public void setPatente(String patente) {
		this.patente = patente;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getAveria() {
		return averia;
	}
	public void setAveria(String averia) {
		this.averia = averia;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Integer getCant_hs() {
		return cant_hs;
	}
	public void setCant_hs(Integer cant_hs) {
		this.cant_hs = cant_hs;
	}
	public vehiculo(String patente, String modelo, String averia, String tipo, Integer cant_hs) {
		super();
		setPatente(patente);
		setModelo(modelo);
		setAveria(averia);
		setTipo(tipo);
		setCant_hs(cant_hs);
	}

	public String mostrar_vehiculo() {
		return "Patente: " + getPatente() + "\nModelo:" + getModelo() + "\nAveria: "
				+ getAveria() + "\nTipo" + getTipo() + "\nHoras estimadas: " + getCant_hs();
	}
   public abstract Double optRecargo();
   
}
