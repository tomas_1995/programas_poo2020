package ejercicio4;

public class auto extends vehiculo {
	private Double recargo;
	
	
	public Double getRecargo() {
		return recargo;
	}


	public void setRecargo(Double recargo) {
		this.recargo = recargo;
	}


	public auto(String patente, String modelo, String averia, String tipo, Integer cant_hs) {
		super(patente, modelo, averia, tipo, cant_hs);
	setRecargo(0.05);
	}
  
   public void mostrar() {
	   System.out.println("Informacion del auto :  ");
	 super.mostrar_vehiculo();

	 
   }


@Override
public Double optRecargo() {
	return this.getRecargo();
	
}
   


}
