package ejercicio4;

public class camion extends vehiculo {
	private Double recargo;

	public Double getRecargo() {
		return recargo;
	}

	public void setRecargo(Double recargo) {
		this.recargo = recargo;
	}

	public camion(String patente, String modelo, String averia, String tipo, Integer cant_hs) {
		super(patente, modelo, averia, tipo, cant_hs);
		setRecargo(0.15);
	}

	public void mostrar() {

		System.out.println("Informacion del camion :  ");

		super.mostrar_vehiculo();
	}

	@Override
	public Double optRecargo() {
		return this.getRecargo();
		
		
	}
}
