
public class ShapeFactory {

	public Shape getShape(String shape) {
		Shape forma;
		switch (shape) {
		case "circle":

			forma = new Circle();

			break;

		case "square":
			forma = new Square();
			break;
		case "rectangule":
			forma = new Rectangule();
			break;
		default:
			throw new IllegalArgumentException("shape");

		}
		return forma;
	}
}
