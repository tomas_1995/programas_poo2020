

import java.util.Scanner;

public class Jugador extends Player {

	private Integer victorias;
	private Integer derrotas;
	private Integer puntuacionTotal;


	public Jugador(String nombre, Integer puntuacion, String respuesta, Integer victorias, Integer derrotas,
			Integer puntuacionTotal) {
		super(nombre, puntuacion, respuesta);
		this.setVictorias(victorias);
		this.setDerrotas(derrotas);
		this.setPuntuacionTotal(puntuacionTotal);
		
	}

	public Integer getVictorias() {
		return victorias;
	}

	public void setVictorias(Integer victorias) { 
		if (victorias < 0) {
		
		}
		this.victorias = victorias;
	}

	public Integer getDerrotas() {
		return derrotas;
	}

	public void setDerrotas(Integer derrotas) {
		this.derrotas = derrotas;
	}

	public Integer getPuntuacionTotal() {
		return puntuacionTotal;
	}

	public void setPuntuacionTotal(Integer puntuacionTotal) {
		this.puntuacionTotal = puntuacionTotal;
	}


	@Override
	public void responder(Pregunta pregunta) {
		Scanner n = new Scanner(System.in);

		if (n.nextLine().equals(pregunta.getRespuesta())) {
			System.out.println("Es correcta");
		} else {
			System.out.println("Es incorrecta");
		}



	

}
}