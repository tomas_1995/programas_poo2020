

public class Pregunta {

	String pregunta;
	String respuesta;

	public Pregunta() {
		super();
		this.setPregunta(pregunta);
		this.setRespuesta(respuesta);
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public void responder(String respuesta) {
		if(this.getRespuesta().equals(respuesta)) {
			System.out.println("Respuesta correcta");
		}else {
			System.out.println("Respuesta incorrecta");
		}
	}
}
