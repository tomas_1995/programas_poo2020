

public abstract class Player {
	private String nombre;
	private Integer puntuacion;
	private String respuesta;

	public Player(String nombre, Integer puntuacion, String respuesta) {
		super();
		this.setNombre(nombre);
		this.setPuntuacion(puntuacion);
		this.setRespuesta(respuesta);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(Integer puntuacion) {
		this.puntuacion = puntuacion;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public abstract void responder(Pregunta pregunta) ;
		
	


}
