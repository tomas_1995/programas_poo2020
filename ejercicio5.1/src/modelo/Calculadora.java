package modelo;

public class Calculadora {
	private Double resultado;
	private Double numero1;
	private Double numero2;
	private String operador = "";

	public Double getResultado() {
		return resultado;
	}

	public void setResultado(Double resultado) {
		this.resultado = resultado;
	}

	public Double getNumero1() {
		return numero1;
	}

	public void setNumero1(Double numero1) {
		this.numero1 = numero1;
	}

	public Double getNumero2() {
		return numero2;
	}

	public void setNumero2(Double numero2) {
		this.numero2 = numero2;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public Double calcular() {

		switch (operador) {
		case "+":
			resultado = numero1 + numero2;
			break;
		case "-":
			resultado = numero1 - numero2;
			break;
		case "*":
			resultado = numero1 * numero2;
			break;
		case "/":
			resultado = numero1 / numero2;
			break;
		case "x^2":
			resultado = Math.pow(numero1, numero2);
			break;
		case "2\u221Ax":
			resultado = Math.sqrt(numero1);
			break;
		case "1/x":
			resultado = 1 / numero1;
			break;
		case "%":
			resultado = (numero1 / numero2) * 100;
			break;

		default: 
			
			break;
		}
		return resultado;
	}

}
