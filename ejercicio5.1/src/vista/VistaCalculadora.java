package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorCalcu;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class VistaCalculadora extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JButton btnPorcentaje;
	private JButton btnCe;
	private JButton btnC;
	private JButton btnFlechas;
	private JButton btnx;
	private JButton btnXelevado;
	private JButton btnRaiz_cuadrado;
	private JButton btnDivision;
	private JButton btnSiete;
	private JButton btnOcho;
	private JButton btnNueve;
	private JButton btnMultiplicacion;
	private JButton btnCuatro;
	private JButton btnCinco;
	private JButton btnSeis;
	private JButton btnResta;
	private JButton btnUno;
	private JButton btnDos;
	private JButton btnTres;
	private JButton btnSuma;
	private JButton btnMasmenos;
	private JButton btnCero;
	private JButton btnPunto;
	private JButton btnIgual;
 private ControladorCalcu controlador;

	public VistaCalculadora() {
		setTitle("CALCULADORA                                       fx-82");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 439, 410);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(21, 11, 388, 73);
		contentPane.add(textField);
		textField.setColumns(10);
		
		btnIgual = new JButton("=");
		btnIgual.setBounds(324, 325, 89, 35);
		contentPane.add(btnIgual);
		
		btnPunto = new JButton(".");
		btnPunto.setBounds(222, 325, 89, 35);
		contentPane.add(btnPunto);
		
		btnCero = new JButton("0");
		btnCero.setBounds(123, 325, 89, 35);
		contentPane.add(btnCero);
		
		btnMasmenos = new JButton("\u00B1");
		btnMasmenos.setBounds(21, 325, 89, 35);
		contentPane.add(btnMasmenos);
		
		btnUno = new JButton("1");
		btnUno.setBounds(21, 279, 89, 35);
		contentPane.add(btnUno);
		
		btnCuatro = new JButton("4");
		btnCuatro.setBounds(21, 233, 89, 35);
		contentPane.add(btnCuatro);
		
		btnSiete = new JButton("7");
		btnSiete.setBounds(21, 187, 89, 35);
		contentPane.add(btnSiete);
		
		btnx = new JButton("1/x");
		btnx.setBounds(21, 141, 89, 35);
		contentPane.add(btnx);
		
		btnPorcentaje = new JButton("%");
		btnPorcentaje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnPorcentaje.setBounds(21, 95, 89, 35);
		contentPane.add(btnPorcentaje);
		
		btnDos = new JButton("2");
		btnDos.setBounds(123, 279, 89, 35);
		contentPane.add(btnDos);
		
		btnCinco = new JButton("5");
		btnCinco.setBounds(123, 233, 89, 35);
		contentPane.add(btnCinco);
		
		btnOcho = new JButton("8");
		btnOcho.setBounds(123, 187, 89, 35);
		contentPane.add(btnOcho);
		
		btnXelevado = new JButton("x^2");
		btnXelevado.setBounds(123, 141, 89, 35);
		contentPane.add(btnXelevado);
		
		btnCe = new JButton("CE");
		btnCe.setBounds(123, 95, 89, 35);
		contentPane.add(btnCe);
		
		btnTres = new JButton("3");
		btnTres.setBounds(222, 279, 89, 35);
		contentPane.add(btnTres);
		
		btnSeis = new JButton("6");
		btnSeis.setBounds(222, 233, 89, 35);
		contentPane.add(btnSeis);
		
		btnNueve = new JButton("9");
		btnNueve.setBounds(222, 187, 89, 35);
		contentPane.add(btnNueve);
		
		btnRaiz_cuadrado = new JButton("2\u221Ax");
		btnRaiz_cuadrado.setBounds(222, 141, 89, 35);
		contentPane.add(btnRaiz_cuadrado);
		
		btnC = new JButton("C");
		btnC.setBounds(222, 95, 89, 35);
		contentPane.add(btnC);
		
		btnSuma = new JButton("+");
		btnSuma.setBounds(324, 279, 89, 35);
		contentPane.add(btnSuma);
		
		btnResta = new JButton("-");
		btnResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnResta.setBounds(320, 233, 89, 35);
		contentPane.add(btnResta);
		
		btnMultiplicacion = new JButton("*");
		btnMultiplicacion.setBounds(320, 187, 89, 35);
		contentPane.add(btnMultiplicacion);
		
		btnDivision = new JButton("/");
		btnDivision.setBounds(320, 141, 89, 35);
		contentPane.add(btnDivision);
		
		btnFlechas = new JButton("<<");
		btnFlechas.setBounds(320, 95, 89, 35);
		contentPane.add(btnFlechas);
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

	public JButton getBtnPorcentaje() {
		return btnPorcentaje;
	}

	public void setBtnPorcentaje(JButton btnPorcentaje) {
		this.btnPorcentaje = btnPorcentaje;
	}

	public JButton getBtnCe() {
		return btnCe;
	}

	public void setBtnCe(JButton btnCe) {
		this.btnCe = btnCe;
	}

	public JButton getBtnC() {
		return btnC;
	}

	public void setBtnC(JButton btnC) {
		this.btnC = btnC;
	}

	public JButton getBtnFlechas() {
		return btnFlechas;
	}

	public void setBtnFlechas(JButton btnFlechas) {
		this.btnFlechas = btnFlechas;
	}

	public JButton getBtnx() {
		return btnx;
	}

	public void setBtnx(JButton btnx) {
		this.btnx = btnx;
	}

	public JButton getBtnXelevado() {
		return btnXelevado;
	}

	public void setBtnXelevado(JButton btnXelevado) {
		this.btnXelevado = btnXelevado;
	}

	public JButton getBtnRaiz_cuadrado() {
		return btnRaiz_cuadrado;
	}

	public void setBtnRaiz_cuadrado(JButton btnRaiz_cuadrado) {
		this.btnRaiz_cuadrado = btnRaiz_cuadrado;
	}

	public JButton getBtnDivision() {
		return btnDivision;
	}

	public void setBtnDivision(JButton btnDivision) {
		this.btnDivision = btnDivision;
	}

	public JButton getBtnSiete() {
		return btnSiete;
	}

	public void setBtnSiete(JButton btnSiete) {
		this.btnSiete = btnSiete;
	}

	public JButton getBtnOcho() {
		return btnOcho;
	}

	public void setBtnOcho(JButton btnOcho) {
		this.btnOcho = btnOcho;
	}

	public JButton getBtnNueve() {
		return btnNueve;
	}

	public void setBtnNueve(JButton btnNueve) {
		this.btnNueve = btnNueve;
	}

	public JButton getBtnMultiplicacion() {
		return btnMultiplicacion;
	}

	public void setBtnMultiplicacion(JButton btnMultiplicacion) {
		this.btnMultiplicacion = btnMultiplicacion;
	}

	public JButton getBtnCuatro() {
		return btnCuatro;
	}

	public void setBtnCuatro(JButton btnCuatro) {
		this.btnCuatro = btnCuatro;
	}

	public JButton getBtnCinco() {
		return btnCinco;
	}

	public void setBtnCinco(JButton btnCinco) {
		this.btnCinco = btnCinco;
	}

	public JButton getBtnSeis() {
		return btnSeis;
	}

	public void setBtnSeis(JButton btnSeis) {
		this.btnSeis = btnSeis;
	}

	public JButton getBtnResta() {
		return btnResta;
	}

	public void setBtnResta(JButton btnResta) {
		this.btnResta = btnResta;
	}

	public JButton getBtnUno() {
		return btnUno;
	}

	public void setBtnUno(JButton btnUno) {
		this.btnUno = btnUno;
	}

	public JButton getBtnDos() {
		return btnDos;
	}

	public void setBtnDos(JButton btnDos) {
		this.btnDos = btnDos;
	}

	public JButton getBtnTres() {
		return btnTres;
	}

	public void setBtnTres(JButton btnTres) {
		this.btnTres = btnTres;
	}

	public JButton getBtnSuma() {
		return btnSuma;
	}

	public void setBtnSuma(JButton btnSuma) {
		this.btnSuma = btnSuma;
	}

	public JButton getBtnMasmenos() {
		return btnMasmenos;
	}

	public void setBtnMasmenos(JButton btnMasmenos) {
		this.btnMasmenos = btnMasmenos;
	}

	public JButton getBtnCero() {
		return btnCero;
	}

	public void setBtnCero(JButton btnCero) {
		this.btnCero = btnCero;
	}

	public JButton getBtnPunto() {
		return btnPunto;
	}

	public void setBtnPunto(JButton btnPunto) {
		this.btnPunto = btnPunto;
	}

	public JButton getBtnIgual() {
		return btnIgual;
	}

	public void setBtnIgual(JButton btnIgual) {
		this.btnIgual = btnIgual;
	}

	public ControladorCalcu getControlador() {
		return controlador;
	}

	public void setControlador(ControladorCalcu controlador) {
		this.controlador = controlador;
	}
}
