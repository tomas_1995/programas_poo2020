package ejemplo;

public interface Accion {
	
	public void comer();
	public void dormir();
	public void estudiar();
	public void setAlumno(Estudiante alumno);
      
}
