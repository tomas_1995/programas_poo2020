package ejemplo;

public class Excepcionejemplo extends Exception {
 
	
	private Integer codigo;
 
	public Excepcionejemplo(Integer codigo) {
		super();
		this.setCodigo(codigo);
		
	}

	@Override
	public String getMessage() {
		
		String mensaje="";
		 
		if (this.getCodigo()== 100){
			mensaje="Letra incorrecta ,intente de nuevo con las opciones A; D;  E;  S";
		}
			
		return mensaje;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	

}
