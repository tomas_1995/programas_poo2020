package ejemplo;

import java.util.Set;

public class Estudiante {
	private Accion alumno;

	public Accion getAlumno() {
		return alumno;
	}

	public void setAlumno(Accion alumno) {
		this.alumno = alumno;
	}

	public Estudiante() {

	  setAccion(new Hambriento());
	  
	}
	
	public void setAccion ( Accion alumno) {
		this.alumno= alumno;
		this.alumno.setAlumno(this);
		
		
	}

	public void alimentar() {
		this.alumno.comer();
	}
	public void dormir() {
		this.alumno.dormir();
	}
	public void estudiar() {
		this.alumno.estudiar();
		
		
	}
}
