package examen2;

public class Excepcion extends Exception { // creo la excepcion personalizada donde actua si el monto total gastado es maximo a un valor determinado
	private Integer cod ;

	public Integer getCod() {
		return cod;
	}

	public void setCod(Integer cod) {
		this.cod = cod;
	}

	public Excepcion(Integer cod) {
		super();
		this.setCod(cod);
	}
	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		String mensaje= "";
		if(this.getCod()==10) {
			mensaje="Excediendo lo que puede gastar de acuerdo al plan Ahorrar ";// mensaje cuando salte la excepcion
		}
		return mensaje;
	

}}
