package examen2;

public class Main { // aplicacion del ejercicio 

	public static void main(String[] args)  {
		// TODO Auto-generated method stub

		Recursocompartido rc = new Recursocompartido(); // creo al recurso compartido
		Empleado1 pepe = new Empleado1(rc);// creo al productor "pepe"
		pepe.setName(" Pepe ");// instancio nombres
		Empleado1 jose = new Empleado1(rc);// creo al productor "jose"
		jose.setName(" Jose ");
		
		try { // encapsulamiento de la excepcion 
		
	
		Cliente tomas = new Cliente(rc);// creo al consumidor "tomas"
		tomas.setName(" Tomas ");// instacion nombre
		tomas.start();// ejecuto al consumidor
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			// TODO: handle exception
		}

		 pepe.start();// ejecuto a los productores
		 jose.start();
		
		 

	}

}
