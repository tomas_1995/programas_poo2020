package examen2;

import java.security.SecureRandom;

public class Empleado1 extends Thread { //"creo la clase  productor empleado1 
	private Recursocompartido caja;

	public Empleado1(Recursocompartido caja) {
		super();
		this.caja = caja;
	}



	public void run() { // metodo run donde creo que el objeto prenda  
		Prenda prenda = new Prenda();
		Integer ta;
		Double pr;
		SecureRandom r = new SecureRandom();

		for (int i = 0; i < r.nextInt(15); i++) {

			switch (r.nextInt(6) + 1) {
			case 1:

				prenda.setMarca("Nike");
				ta = r.nextInt(50) + 5;
				prenda.setTalle(ta);
				prenda.setTipo("Pantalon");
				pr = 1500 + (6000 - 1500) * r.nextDouble();
				prenda.setPrecio(pr);
				prenda.setCodigo(1010);
				this.getCaja().almaceno(prenda);// es el bufer ,lugar donde almaceno los objetos creados por el empleado
				break;
			case 2:

				prenda.setMarca("Nike");
				ta = r.nextInt(50) + 5;
				prenda.setTalle(ta);
				prenda.setTipo("Remera");
				pr = 500 + (3000 - 500) * r.nextDouble();
				prenda.setPrecio(pr);
				prenda.setCodigo(1110);
				this.getCaja().almaceno(prenda);

				break;
			case 3:

				prenda.setMarca("Nike");
				ta = r.nextInt(50) + 5;
				prenda.setTalle(ta);
				prenda.setTipo("Zapatillas");
				pr = 6500 + (16000 - 6500) * r.nextDouble();
				prenda.setPrecio(pr);
				prenda.setCodigo(1111);
				this.getCaja().almaceno(prenda);

				break;
			case 4:

				prenda.setMarca("Adidas");
				ta = r.nextInt(50) + 5;
				prenda.setTalle(ta);
				prenda.setTipo("Pantalon Gimnasias");
				pr = 1000 + (6000 - 1000) * r.nextDouble();
				prenda.setPrecio(pr);
				prenda.setCodigo(1010);
				this.getCaja().almaceno(prenda);

				break;
			case 5:

				prenda.setMarca("Nike");
				ta = r.nextInt(50) + 5;
				prenda.setTalle(ta);
				prenda.setTipo("Zapatillas vestir");
				pr = 4000 + (10000 - 4000) * r.nextDouble();
				prenda.setPrecio(pr);
				prenda.setCodigo(1111);
				this.getCaja().almaceno(prenda);

				break;

			default:
				prenda.setMarca("Lacoste");
				ta = r.nextInt(50) + 5;
				prenda.setTalle(ta);
				prenda.setTipo("Zapatillas vestir");
				pr =10000 + (20000 - 10000) * r.nextDouble();
				prenda.setPrecio(pr);
				prenda.setCodigo(1111);
				this.getCaja().almaceno(prenda);

			}

		}
		
		
		this.getCaja().almaceno(null);// si no creo ningun objeto ,directamente envio un null para indicarlo con esa linea de codigo

	}
	public Recursocompartido getCaja() {
		return caja;
	}

	public void setCaja(Recursocompartido caja) {
		this.caja = caja;
	}
}