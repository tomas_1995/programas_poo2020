package examen2;

import java.util.ArrayList;

public class Recursocompartido {
	private ArrayList<Prenda> lista;

	public Recursocompartido() {
		super();
		this.lista = new ArrayList<Prenda>();
	}

	public synchronized void almaceno(Prenda prenda) { // metodo sincronizado donde actua el productor ...esperara si esta lleno el bufer y creara y almacenará si hay lugar

		while (this.getLista().size() >= 2) {
			try {
				Thread.sleep(1000);

				System.out.println("Almacen lleno");
				Thread.sleep(1000);
				System.out.println("");
				System.out.println(
						"El Empleado" + Thread.currentThread().getName() + " esta esperando espacio de almacenamiento");//notificacion por consola como es la situacion del hilo en ejecucion
				System.out.println("");
				wait();

			} catch (InterruptedException e) {
				e.printStackTrace();
				// TODO: handle exception

			}
		}

		this.getLista().add(prenda); // agrega a la lista(bufer) la prenda creada
		notifyAll();// notifica al procesador que termino el hilo 
		if (prenda != null) {
			System.out.println("");
			System.out.println(
					"El Empleado" + Thread.currentThread().getName() + " almaceno la prenda  " + prenda.getMarca());
			System.out.println("");
		}
	}

	public synchronized Prenda retiro() {// metodo donde el cliente(consumidor ) retira la prenda del bufer 
		while (this.getLista().size() == 0) {
			try {
				Thread.sleep(1000);
				System.out.println("Almacen vacio, Esperando...");
				System.out.println("");
				Thread.sleep(1000);

				System.out.println("Cliente" + Thread.currentThread().getName() + "esta esperando prenda en almacen ");// indica el estado de la hilo consumidor"cliente"
				System.out.println("");
				wait();

			} catch (InterruptedException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		Prenda compro = this.getLista().remove(0);// remueve del bufer(lista ) la prenda

		if (compro != null) {
			System.out.println("");
			System.out.println("El Cliente " + Thread.currentThread().getName() + "  compro la prenda");// indica el estado de la hilo consumidor"cliente"
			System.out.println("");
		}

		notifyAll();
		return compro;

	}

	public ArrayList<Prenda> getLista() {
		return lista;
	}

	public void setLista(ArrayList<Prenda> lista) {
		this.lista = lista;
	}
}
