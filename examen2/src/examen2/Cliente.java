package examen2;

import java.util.ArrayList;

public class Cliente extends Thread {// creo el consumidor que extiende de  la clase Thread ( creo que hilo consumidor)
	private Recursocompartido Caja; // creo al recurso compartido "Caja" 
	private ArrayList<Prenda> prendas;// Array donde se almacenan los objetos que el cliente consume

	public Cliente(Recursocompartido caja) throws Excepcion {
		super();
		this.Caja = caja;
		this.prendas = new ArrayList<Prenda>();
	}

	public void run() { // metodo Run metodo donde se aplica los metos del recurso compartido

		Integer contador = 0;
		Prenda prenda11;

		while (contador < 2) { // interacion que determina si el empleado dejo de reponer (producir)
			prenda11 = this.getCaja().retiro();// retiro del bufer el objeto creado por el empleado 
			if (prenda11 != null) {

				getPrendas().add(prenda11);// al elemento retirado del bufer lo agrego al array del consumidor

			} else {
				contador++; // cuento la cantidad de veces que aparecen  objetos null en el array del empleado

			}

		}
		try {
			generoInforme(); // llamo al metodo que genera el informe 
		} catch (Excepcion e) { // propagacion de la excepcion 
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

	}

	public void generoInforme() throws Excepcion { // genero el informe  donde muestro los productos consumidos ,etc.
		Integer cant = 0;
		Integer cat = 0;
		Integer cants = 0;
		Double preciototal = 0.00;
		System.out.println("/////////////////////////////");
		System.out.println("Ticket Prendas vendidas  ");
		System.out.println("");
		System.out.println("Prendas ");
		System.out.println("");
		System.out.println("*******************");
		for (Prenda prenda : prendas) {

			System.out.println("Prenda  : " + "Marca  " + prenda.getMarca() + "  Tipo: " + prenda.getTipo()
					+ "  Talle :  " + prenda.getTalle() + " Prencio : $ " + prenda.getPrecio());

			preciototal = preciototal + prenda.getPrecio();

			if (prenda.getCodigo() == 1111) { // cuento la cantidad del prendas de cada tipo fueron compradas 
				//( podria haber implementado hashmap pero no lo vi necesario ya que solo eran tres codigos distintos)
				cant = cant + 1;

			} else if (prenda.getCodigo() == 1110) {
				cat = cat + 1;

			} else
				cants = cants + 1;

		}
		System.out.println("*******************");
		System.out.println("");
		System.out.println("//////////////////////////////////////////");
		System.out.println("Prendas Seleccionadas ");
		System.out.println(""); 
		System.out.println( cant + " Pares de Calzado");
		System.out.println(cat + " Remeras ");
		System.out.println( cants + " Pantalones");
			
		System.out.println("Monto Total : $" + preciototal);
		if (preciototal > 40000.00) {// sitio donde controlo el monto total y lugar donde podria saltar la excepcion
			System.out.println("No podes Pagar");
			throw new Excepcion(10);
		
		}
		// Al salta la excepcion "el cliente no puede pagar",por lo tanto no se vende nada ,
		//entonces la parte del informe donde muestro lo que se vendio y cantidades de cada una de las prendas ,
		//no se muestran
		System.out.println("Cliente Pago ");
		System.out.println("Se   Vendi� : ");
		System.out.println( cant + " Pares de Calzado");
		System.out.println(cat + " Remeras ");
		System.out.println( cants + " Pantalones");
		System.out.println("");
		System.out.println("//////////////////////////////////////////");
	}

	public Recursocompartido getCaja() {
		return Caja;
	}

	public void setCaja(Recursocompartido caja) {
		Caja = caja;
	}

	public ArrayList<Prenda> getPrendas() {
		return prendas;
	}

	public void setPrendas1(ArrayList<Prenda> prendas) {
		this.prendas = prendas;
	}
}
