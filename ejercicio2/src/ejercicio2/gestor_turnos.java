package ejercicio2;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class gestor_turnos {

	private Integer hora;
	private paciente paciente1;
	private ArrayList<paciente> pacientes = new ArrayList<paciente>();

	public Integer getHora() {
		return hora;
	}

	public void setHora(Integer hora) {
		this.hora = hora;
	}

	public paciente getPaciente1() {
		return paciente1;
	}

	public void setPaciente1(paciente paciente1) {
		this.paciente1 = paciente1;
	}

	public ArrayList<paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(ArrayList<paciente> pacientes) {
		this.pacientes = pacientes;
	}

	public void agregarPaciente() {

		Scanner vista = new Scanner(System.in);
		String nombre;
		String apellido;
		Character sexo;
		Integer edad;
		
        
					

				System.out.println("Ingrese nombre paciente: ");
				nombre = vista.nextLine();

				System.out.println("Ingrese apellido paciente: ");
				apellido = vista.nextLine();

				System.out.println("Ingrese sexo: Masculino- Femenino");

				sexo = vista.nextLine().charAt(0);

				System.out.println("Ingrese edad: ");
				edad = vista.nextInt();

				paciente paciente1 = new paciente(nombre, apellido, sexo, edad);

				System.out.println("El peso es: " + paciente1.getPeso());
				System.out.println("La altura es: " + paciente1.getAltura());

				pacientes.add(paciente1);// se a�ade al arreglo al paciente
				System.out.println("Nuevo paciente agregado");

				

			} 
		}
	

	public gestor_turnos() {
		super();
		this.pacientes = new ArrayList<paciente>();
	}

	public void listarPacientes() {
		for (paciente paciente : pacientes) {
			System.out.println("n/Nombre: " + paciente.getNombre() + "n/Apellido: " + paciente.getApellido());

		}

	}

	public void asignar_paciente() {
// metodo sacado de internet para armar y desordenar un arreglo con numeros
		Integer x = 8;
		int[] horarios_turnos = new int[12];
		for (int i = 0; i < 12; i++) {

			horarios_turnos[i] = x;
			x = x + 1;
		}
		Random r = new Random();

		for (int i = 12; i > 1; i--) {
			int posicion = r.nextInt(i);
			int tmp = horarios_turnos[i - 1];
			horarios_turnos[i - 1] = horarios_turnos[posicion];
			horarios_turnos[posicion] = tmp;

		}

		Integer m = 0;
		for (paciente paci : pacientes) {

			System.out.println("Apellido:a" + paci.getApellido() + "  Nombre:" + paci.getNombre() + "  Hora: "
					+ horarios_turnos[m] + "hs");
			m = m + 1;

		}

	}

	public void informe_altura() {

		Scanner respues = new Scanner(System.in);
		Character rtas;

		Double cantpaciente = 0.0;
		Double promaltura = 0.0;
		Double promtotal = 0.0;

		System.out.println("Seleccione sexo para calcular promedio: M (masculino) o F(femenino)");

		rtas = respues.nextLine().charAt(0);

		for (paciente paciente : pacientes) {

			if (paciente.getSexo() == rtas) {

				cantpaciente = cantpaciente + 1.0;
				promaltura = promaltura + paciente.getAltura();

			}

		}
		promtotal = (promaltura / cantpaciente);
		System.out.println("La altura promedio en funcion del sexo:" + rtas + "es igual a : " + promaltura);
	}

	public void informe_peso() {

		Scanner respuesta = new Scanner(System.in);
		Integer rtas;

		Double cantpacientes = 0.0;
		Double prompeso = 0.0;
		Double promtotalpeso = 0.0;

		System.out.println("Seleccione la edad para calcular promedio: ");

		rtas = respuesta.nextInt();

		for (paciente paciente : pacientes) {

			if (paciente.getEdad() == rtas) {

				cantpacientes = cantpacientes + 1.0;
				prompeso = prompeso + paciente.getPeso();

			}

		}
		promtotalpeso = (prompeso / cantpacientes);
		System.out.println("El peso promedio en funcion de la edad:" + rtas + "es igual a : " + prompeso);
	}

}
