
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Window.Type;
import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JLabel;

public class buscarImagen extends JFrame {

	private JPanel lblFondo;
	private JTextField txtDireccion;
	private JPanel lblVista;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				try {
					buscarImagen frame = new buscarImagen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public buscarImagen() {
		setForeground(Color.BLUE);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(buscarImagen.class.getResource("/javax/swing/plaf/basic/icons/JavaCup16.png")));
		setTitle("Visualizacion");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 597, 417);
		lblFondo = new JPanel();
		lblFondo.setBackground(new Color(245, 222, 179));
		lblFondo.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(lblFondo);
		lblFondo.setLayout(null);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				mostrar();
				

			}
		});
		btnBuscar.setFont(new Font("Segoe UI Black", Font.PLAIN, 11));
		btnBuscar.setBounds(420, 346, 89, 23);
		lblFondo.add(btnBuscar);

		txtDireccion = new JTextField();
		txtDireccion.setBounds(179, 347, 231, 20);
		lblFondo.add(txtDireccion);
		txtDireccion.setColumns(10);

		JLabel lblDireccionURL = new JLabel("Direccion URL :");
		lblDireccionURL.setFont(new Font("Snap ITC", Font.BOLD, 16));
		lblDireccionURL.setLabelFor(txtDireccion);
		lblDireccionURL.setBounds(10, 353, 179, 14);
		lblFondo.add(lblDireccionURL);
		
		JLabel lblVista = new JLabel("");
		lblVista.setBounds(10, 11, 561, 324);
		lblFondo.add(lblVista);
		
		

	}
		protected void mostrar( ) {

			ImageIcon imageIcon = new ImageIcon(new ImageIcon(txtDireccion.getText()).getImage().getScaledInstance(561,324, Image.SCALE_DEFAULT));
			
			JLabel lblVista = new JLabel("");
			lblVista.setBounds(10, 11, 561, 324);	
			lblVista.setIcon(imageIcon);
			lblFondo.add(lblVista);
			
			
			
         repaint();
}

}
/*
 se podria controlar la url chequeando si es un directorio y si el archivo existe previamente asi: 
  File file = new File(txtDireccion.getText());
if (!file.isDirectory())
   file = file.getParentFile();
if (file.exists()){
    ...
}*/
 